#!/bin/sh
# Tcl ignores the next line -*- tcl -*- \
exec wish "$0" -- "$@"

######################################################
#        Rac GUI
# 1C RAC (tm) grafical user interface
# Distributed under GNU Public License
# Author: Sergey Kalinin svk@nuk-svk.ru
# Home page: https://nuk-svk.ru
# Git repos: https://bitbucket.org/svk28/rac-gui
#
# Version: 1.1.0
# Release: 2
#
######################################################


# определим текущую версию и релиз
set f [open $argv0 "RDONLY"]
while {[gets $f line] >=0} {
    if [regexp -nocase -all -- {version: ([0-9]+?.[0-9]+?.[0-9]+?)} $line match v1] {
        set racgui_version $v1
    }
    if [regexp -nocase -all -- {release: ([0-9]+?)} $line match v1] {
        set racgui_release $v1
    }
}
close $f

package require msgcat

# Устанавливаем текущий каталог
set dir(root) [pwd]

set dir(doc) [file join $dir(root) doc]

# Устанавливаем рабочий каталог, если его нет то создаём.
# Согласно спецификации XDG проверяем наличие переменных и каталогов
if [info exists env(XDG_CONFIG_HOME)] {
    set dir(work) [file join $env(XDG_CONFIG_HOME) rac_gui]
} elseif [file exists [file join $env(HOME) .config]] {
    set dir(work) [file join $env(HOME) .config rac_gui]
} else {
    set dir(work) [file join $env(HOME) .rac_gui]
}

if {[file exists $dir(work)] == 0} {
    file mkdir $dir(work)    
}
# Проверяем старые конфиги и при наличии переносим
if {[file exists [file join $env(HOME) .rac_gui]] ==1 && $dir(work) ne [file join $env(HOME) .rac_gui]} {
    cd [file join $env(HOME) .rac_gui]
    foreach f [glob -type f *] {
        if {![file exists [file join $dir(work) $f]]} {
            puts "File copy - [file copy $f $dir(work)]"
            file rename $f [list $f old]
        }
    }
    cd $env(HOME)
    #file rename -force [file join $env(HOME) .rac_gui] [file join $env(HOME) .rac_gui.old]
    file delete -force [file join $env(HOME) .rac_gui]
    tk_messageBox -message "[::msgcat::mc "All files from old working dir will be moved into new location:"]\n$dir(work)" -icon info -type ok 
}



puts "Work dir is $dir(work)"

# каталог с модулями
set dir(lib) "[file join $dir(root) lib]"

# загружаем пользовательский конфиг, если он отсутствует, то копируем дефолтный
if {[file exists [file join $dir(work) rac_gui.cfg]] ==0} {
    file copy [file join $dir(root) rac_gui.cfg] [file join $dir(work) rac_gui.cfg]
} 
source [file join $dir(work) rac_gui.cfg]

::msgcat::mclocale $default(locale)
::msgcat::mcload [file join $dir(lib) msg]


set cluster_user ""
set cluster_pwd ""
set agent_user ""
set agent_pwd ""
## LOAD FILE ##
# Загружаем модули кроме gui.tcl так как его надо загрузить последним
foreach modFile [lsort [glob -nocomplain [file join $dir(lib) *.tcl]]] {
    if {[file tail $modFile] ne "gui.tcl"} {
        source $modFile
        puts "Loaded module $modFile"
    }
}
source [file join $dir(lib) gui.tcl]
source [file join $dir(work) rac_gui.cfg]

CheckVariablesSet

# Читаем файл со списком серверов 1С
#set serversList [dict create servers]
if [file exists [file join $dir(work) 1c_srv_new.cfg]] {
    set f_new [open [file join $dir(work) 1c_srv_new.cfg] "RDONLY"]
    set str ""
    while {[gets $f_new line] >=0} {
        append str " [string trim $line]"
    }
    if {$str ne ""} {
        set str [string map {"\{ " "\{" " \}" "\}"} $str]
        dict set servers_list servers $str
        close $f_new
        puts $servers_list
        puts [dict get $servers_list servers]
        dict for {host host_data} [dict get $servers_list servers] {
            .frm_tree.tree insert {} end -id "server::$host" \
            -text [dict get $host_data name] -values "$host"
            puts "$host > $host_data"
            puts [dict get $host_data name]
            set rac_cmd_for_host($host) [dict get $host_data rac_cmd]
        }
    } else {
        set servers_list ""
    }
} else {
    set servers_list ""
}

