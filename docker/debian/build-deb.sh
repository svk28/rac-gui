#!/bin/bash

cd rac-gui

VERSION=$(grep Version rac_gui.tcl | grep -oE '\b[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,2}\b')
RELEASE=$(grep Release rac_gui.tcl | grep -oE '\b[0-9]{1,3}\b')

mv rac_gui.tcl racgui

sed -i "s+^set\ dir(lib)+set\ dir(lib)\ /usr/share/rac-gui/lib ;#+g" racgui
   
sed -i "s+\[pwd\]+/usr/share/rac-gui+g" racgui
   
tar czf ../rac-gui_${VERSION}.orig.tar.gz .

dpkg-buildpackage

cp ../rac-gui_${VERSION}-${RELEASE}_amd64.deb /files/
