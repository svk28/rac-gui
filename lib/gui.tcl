######################################################
#                Rac GUI
# Distributed under GNU Public License
# Author: Sergey Kalinin svk@nuk-svk.ru
# Copyright (c) "http://nuk-svk.ru" 2018
# https://bitbucket.org/svk28/rac-gui
######################################################

# установка размера и положения основного окна
# можно установить в переменную topLevelGeometry в конфиг программы
if {[info exists topLevelGeometry]} {
    wm geometry . $topLevelGeometry
} else {
    wm geometry . 1024x768
}
# Заголовок окна
wm title . "1C Rac GUI"
wm iconname . "1C Rac Gui"
# иконка окна (берется из файла lib/imges.tcl)
wm iconphoto . tcl
wm protocol . WM_DELETE_WINDOW Quit
wm overrideredirect . 0
wm positionfrom . user

bind . <Control-q> Quit
bind . <Control-Q> Quit
bind . <Control-eacute> Quit
bind . <Insert> Add
bind . <Delete> Del
bind . <Control-Return> Edit
bind . <F1> ShowHelpDialog

#ttk::style configure TPanedwindow -background blue
#ttk::style configure Sash -sashthickness 5
#ttk::style configure TButton  -padding 60  -relief flat -bg black
#ttk::style configure Custom.Treeview -foreground red
#ttk::style configure Custom.Treeview -rowheight 20


if [info exists default(theme)] {
    ttk::style theme use $default(theme)
}

# Панель инсрументов
set frm_tool [ttk::frame .frm_tool]
pack $frm_tool -side left -fill y 
ttk::panedwindow .panel -orient horizontal -style TPanedwindow
pack .panel -expand true -fill both
pack propagate .panel false

ttk::button $frm_tool.btn_add  -command Add  -image add_grey_32
ttk::button $frm_tool.btn_del  -command Del -image del_grey_32
ttk::button $frm_tool.btn_edit  -command Edit -image edit_grey_32
ttk::button $frm_tool.btn_help -command ShowHelpDialog -image question_grey_32
ttk::button $frm_tool.btn_quit -command Quit -image quit_grey_32

pack $frm_tool.btn_add $frm_tool.btn_del $frm_tool.btn_edit $frm_tool.btn_help -side top -padx 5 -pady 5
#label $frm_tool.lbl_logo -image tcl
pack $frm_tool.btn_quit -side bottom -padx 5 -pady 5
#pack $frm_tool.lbl_logo -side bottom -padx 5 -pady 5

# Дерево с полосами прокрутки
set frm_tree [ttk::frame .frm_tree]

ttk::scrollbar $frm_tree.hsb1 -orient horizontal -command [list $frm_tree.tree xview]
ttk::scrollbar $frm_tree.vsb1 -orient vertical -command [list $frm_tree.tree yview]
set tree [ttk::treeview $frm_tree.tree -show tree \
-xscrollcommand [list $frm_tree.hsb1 set] -yscrollcommand [list $frm_tree.vsb1 set]]

grid $tree -row 0 -column 0 -sticky nsew
grid $frm_tree.vsb1 -row 0 -column 1 -sticky nsew
grid $frm_tree.hsb1 -row 1 -column 0 -sticky nsew
grid columnconfigure $frm_tree 0 -weight 1
grid rowconfigure $frm_tree 0 -weight 1

# назначение обработчика нажатия кнопкой мыши
#bind $frm_tree.tree <ButtonRelease> "TreePress %x %y %X %Y $frm_tree.tree"
bind $frm_tree.tree <ButtonRelease> "TreePress $frm_tree.tree"

# Список для данных (таблица)
set frm_work [ttk::frame .frm_work]
ttk::scrollbar $frm_work.hsb -orient horizontal -command [list $frm_work.tree_work xview]
ttk::scrollbar $frm_work.vsb -orient vertical -command [list $frm_work.tree_work yview]
set tree_work [
    ttk::treeview $frm_work.tree_work \
    -show headings  -columns "par val" -displaycolumns "par val"\
    -xscrollcommand [list $frm_work.hsb set] \
    -yscrollcommand [list $frm_work.vsb set]
]
# table rows background colors

if {[info exists default(theme)] == 1} {
    $tree_work tag configure dark -background $color(dark_table_bg)
    $tree_work tag configure light -background $color(light_table_bg)
}
bind $tree_work <Double-ButtonPress-1> Edit

$tree tag configure selected -background white  -foreground black
$tree_work tag configure selected -background white  -foreground black

bind $tree_work <ButtonRelease-1> {
    $tree_work tag remove selected
    $tree_work item [.frm_work.tree_work selection] -tags selected
}


#$tree_work heading par -text "Параметр" -anchor center
#$tree_work heading val -text "Значение" -anchor center
#set tree_work [ttk::treeview $frm_work.tree_work \

#$tree_work heading Creator -text "Creator" -anchor center
#$tree_work heading Year -text "Year" -anchor center
# Пазмещение элементов на форме
grid $tree_work -row 0 -column 0 -sticky nsew
grid $frm_work.vsb -row 0 -column 1 -sticky nsew
grid $frm_work.hsb -row 1 -column 0 -sticky nsew
grid columnconfigure $frm_work 0 -weight 1
grid rowconfigure $frm_work 0 -weight 1
#pack $tree_work -expand true -fill both
pack $frm_tree $frm_work -side left -expand true -fill both

#.panel add $frm_tool -weight 1
.panel add $frm_tree -weight 1 
.panel add $frm_work -weight 1

